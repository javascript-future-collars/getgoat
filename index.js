const button = document.createElement("button");
button.id = "getGoat";
button.innerText = "Get Goat";
document.body.appendChild(button);

const getGoat = (url) => {
  const img = document.createElement("img");
  img.src = url;
  img.setAttribute("data-test", "img-goat");
  document.body.appendChild(img);
};

const getURL = (width, height) => {
  const url = `https://placegoat.com/${width}/${height}`;
  fetch(url)
    .then((response) => response.json())
    .then((data) => getGoat(data[0].url));
};

button.addEventListener("click", getURL(300, 200));
